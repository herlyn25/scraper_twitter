import matplotlib.pyplot as plt
import pandas as pd

path = "data.csv"


def get_data(my_path):
    df_list = []
    try:
        df = pd.read_csv(my_path)
        columns_dict = [x for x in df.columns]
        for i in df.index.values:
            df_line = df.loc[i, columns_dict].to_dict()
            df_list.append(df_line)

    except FileNotFoundError:
        print("Dont found file")

    return df_list

def count_tweet(array):
    counter = len(array)
    print(f' Hay {counter} tweets')


def count_tweet_by_date(array):
    """
    :param array: contain a list of dicts
    :return: a tuple of lists, in position [0] the list contain the dates of tweets and the position [1]
    contain a list with numbers repetitions.
    """
    list_date = []
    list_count_date = []
    count_date = 0
    for item in array:
        if item['date'] not in list_date:
            list_date.append(item['date'])
            count_date = 0
    for item_date in list_date:
        for item in array:
            if item_date == item['date']:
                count_date += 1
        list_count_date.append(count_date)
        count_date = 0
    #print(list_date)
    return list_date, list_count_date


def count_hashtags_searched_word(array, searched_word):
    count_word = 0
    list_hashtag = []
    if type(searched_word) == list:
        for word in searched_word:
            for item in array:
                string_array = (item['hashtags'].lstrip('[').replace(']', '').replace("'", "").replace(',', ''))
                for string in string_array.split():
                    if word.lower() in string:
                        if string not in list_hashtag:
                            list_hashtag.append(string)
    elif type(searched_word) == str:
        for item in array:
            string_array = (item['hashtags'].lstrip('[').replace(']', '').replace("'", "").replace(',', ''))
            for string in string_array.split():
                if searched_word.lower() in string:
                    if string not in list_hashtag:
                        list_hashtag.append(string)
    return list_hashtag


def count_hashtags_searched_word2(array, searched_word):
    count_word = 0
    if type(searched_word) == list:
        for word in searched_word:
            for item in array:
                count_word += item['hashtags'].count(word.lower())
    elif type(searched_word) == str:
        for item in array:
            count_word += item['hashtags'].count(searched_word.lower())
    return count_word


def count_mentions_word(array, words):
    counter_word = 0
    if type(words) == str:
        for item in array:
            counter_word += item['mentions'].count(words.lower())
        print(f"{words} is {counter_word} times mentioned")
    elif type(words) == list:
        for word in words:
            for item in array:
                counter_word += item['mentions'].count(word.lower())
        print(f"{words} is {counter_word} times mentioned")


def bar_graphic(array1, array2):
    plt.style.use("bmh")  # Declaración del estilo#Opción 1
    fig, ax = plt.subplots()  # Creación de una figura con un axis#Opción 2
    

    plt.plot()
    plt.bar(array1, array2)
    plt.show()
    plt.close('all')


def menu():
    flag=True
    my_array = get_data(path)
    tuple_data = count_tweet_by_date(my_array)
    datos = ['James', 'seleccioncolombia']
    while(flag):
        print("""
        1. Count mentions (searched word)
        2. Count tweets (total)
        3. Find hashtags (It contain searched words)
        4. Count tweets by date (day)
        5. exit    
        """)
        option = input("Digit a valid option(1-4):")
        if option == '1':
            count_mentions_word(my_array, datos)
        elif option == '2':
            count_tweet(my_array)
        elif option == '3':
            list_hashtags = count_hashtags_searched_word(my_array, datos)
            if len(list_hashtags) == 0:
                print(f'The words {datos} has not hashtags where It was mentioned')
            else:
                print(f'The words {datos} is in the follows hashtags {list_hashtags}')
        elif option == '4':
            bar_graphic(tuple_data[0], tuple_data[1])
            print(dict(zip(tuple_data[0],tuple_data[1])))
        elif option == '5':
            print(" Finished Program")
            flag=False


if __name__ == "__main__":
    menu()