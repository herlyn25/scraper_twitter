import twint
from textblob import TextBlob

c = twint.Config()
path = "data.csv"
c.Search = ['James', 'seleccioncolombia']  # topic
c.Limit = 200  # number of Tweets to scrape
c.Store_csv = True  # store tweets in a csv file
c.since = '2021-01-01'
c.Output = path  # path to csv file

twint.run.Search(c)

pos_count = 0
pos_correct = 0

with open(path, "r") as f:
    for line in f.read().split('\n'):
        analysis = TextBlob(line)
        print(line)
        try:
            eng = analysis.translate(to='en')
            if eng.sentiment.polarity >= 0:
                pos_correct += 1
            pos_count += 1
            #print(pos_count)
        except:
            pass
            # Mostramos este mensaje en caso de que se presente algún problema
            # print("El elemento no está presente")

neg_count = 0
neg_correct = 0

with open(path, "r") as f:
    for line in f.read().split('\n'):
        analysis = TextBlob(line)
        # print(line)
        try:
            eng = analysis.translate(to='en')
            if eng.sentiment.polarity < 0:
                neg_correct += 1
            neg_count += 1
            #print(neg_count)
        except:
            pass
            # print('el elemento no esta presente')

print("Precisión positiva = {}%".format(pos_correct / pos_count * 100.0))
print("Precisión negativa = {}%".format(neg_correct / neg_count * 100.0))
